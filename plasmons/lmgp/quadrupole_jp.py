#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from dftpy.formats import io
import ase
import ase.io

from dftpy.grid import DirectGrid
from dftpy.field import DirectField
from dftpy.functional import Functional, TotalFunctional
from dftpy.td.propagator import Propagator
from dftpy.td.hamiltonian import Hamiltonian
from dftpy.utils.utils import calc_rho, calc_j
from dftpy.td.utils import initial_kick
from dftpy.ions import Ions
from dftpy.td.utils import PotentialOperator
from dftpy.functional.external_potential import ExternalPotential
# from dftpy.functional.scaled_functional import ScaledFunctional
from dftpy.optimize import Dynamics
from dftpy.mpi import MP, sprint

mp = MP(parallel=True)
import os
os.chdir('../')
path_file = os.getcwd()

inputfile = 'Ag20_rotate.vasp'
atoms = ase.io.read(inputfile, format='vasp')
#rho_ks = io.read_density('KS_TFvW/rho_Ag_50.xsf', full=True)
#nr = rho_ks.grid.nr
nr = [144, 144, 144]

path = '/projectsn/mp1009_1/Valeria/NLPP/PP/'
PP_list = {'Ag':path+'Ag_OEPP_PZ.UPF'}
ions = Ions.from_ase(atoms)
grid = DirectGrid(ions.cell, nr, mp=mp, full=True)
nelec = 20
rho_ini = DirectField(grid=grid)
rho_ini[:] = nelec / grid.volume

ke = Functional(type='KEDF',name='LMGP')
xc = Functional(type='XC',name='LDA')
hartree = Functional(type='HARTREE')
pseudo = Functional(type='PSEUDO', grid=grid, ions=ions, PP_list=PP_list)
dyn = Functional(type='DYNAMIC', name='JP1')
totalfunctional = TotalFunctional(KineticEnergyFunctional=ke,
                                XCFunctional=xc,
                                HARTREE=hartree,
                                PSEUDO=pseudo,
                                Nonadiabatic=dyn
                                 )
vw = Functional(type='KEDF', name='Vw')

ke.options.update({'y':0})

rho_ks = io.read_density('qe_eopp/rho_Ag20.xsf', grid=grid, full=True)

ext = hartree(rho_ks).potential+xc(rho_ks).potential + pseudo(rho_ks).potential + vw(rho_ks).potential+ ke(rho_ks).potential
# ext[rho_ks<1E-4] = 0.0
# io.write_potential('KS_TFvW/vs_ks.snpy',data = ext, ions = ions)
#io.write_potential('KS_TFvW/vs_ks_eopp.snpy',data = ext, ions = ions)


#ext_load = io.read_potential('KS_TFvW/vs_ks_eopp.snpy', grid = rho_ks.grid)
#ext_load = io.read_potential('vs_ks.snpy', grid = rho_ks.grid)
ext = ExternalPotential(v=-ext)
totalfunctional.UpdateFunctional(newFuncDict={'EXT': ext})

direction = 0 # 0, 1, 2 means x, y, z-direction, respectively
k = 1.0e-3 # kick_strength in a.u.
psi = initial_kick(k, direction, np.sqrt(rho_ks))
j0 = calc_j(psi)
potential = totalfunctional(rho_ks, current=j0, calcType=['V']).potential
interval = 0.041341374575751

class Runner(Dynamics):

    def __init__(self, rho0, totalfunctional, k, direction, interval, max_steps):
        super(Runner, self).__init__()
        self.max_steps = max_steps
        self.totalfunctional = totalfunctional
        self.rho0 = rho0
        self.rho = rho0
        self.psi = initial_kick(k, direction, np.sqrt(self.rho0))
        self.j = calc_j(self.psi)
        potential = self.totalfunctional(self.rho0, current=self.j, calcType=['V']).potential
        hamiltonian = Hamiltonian(v=potential)
        self.prop = Propagator(hamiltonian, interval, name='crank-nicholson')
        self.dipole = []
        self.quadrupole = []
        self.attach(self.calc_dipole) # this attaches the calc_dipole function to the observers list which runs after each time step.
        self.attach(self.calc_quadrupole)

    def step(self):
        sprint(self.psi.integral(), comm=self.rho.grid.mp)
        self.psi, info = self.prop(self.psi)
        self.rho = calc_rho(self.psi)
        self.j = calc_j(self.psi)
        potential = self.totalfunctional(self.rho, current=self.j, calcType=['V']).potential
        self.prop.hamiltonian.v = potential

    def calc_dipole(self):
        delta_rho = self.rho - self.rho0
        delta_mu = (delta_rho * delta_rho.grid.r).integral()
        sprint(len(self.dipole), delta_mu, comm=self.rho.grid.mp)
        self.dipole.append(delta_mu)

    def calc_quadrupole(self):
        delta_rho = self.rho - self.rho0
        q = np.zeros((3,3))
        for i in np.arange(3):
            for j in np.arange(3):
                if j==i:
                    delta = 1
                else:
                    delta = 0
                q[i][j] = ((3 * delta_rho.grid.r[i] * delta_rho.grid.r[j] - (delta_rho.grid.r**2).sum(axis=0) * delta) * delta_rho).integral()
        sprint(len(self.quadrupole), q, comm=self.rho.grid.mp)
        self.quadrupole.append(q)

from dftpy.td.predictor_corrector import PredictorCorrector


class Runner2(Dynamics):

    def __init__(self, rho0, totalfunctional, k, direction, interval, max_steps):
        super(Runner2, self).__init__()
        self.max_steps = max_steps
        self.totalfunctional = totalfunctional
        self.rho0 = rho0
        self.rho = rho0
        self.psi = initial_kick(k, direction, np.sqrt(self.rho0))
        self.j = calc_j(self.psi)
        potential = self.totalfunctional(self.rho0, current=self.j, calcType=['V']).potential
        hamiltonian = Hamiltonian(v=potential)
        self.prop = Propagator(hamiltonian, interval, name='crank-nicholson')
        self.dipole = []
        self.attach(self.calc_dipole)
        self.quadrupole = []
        self.attach(self.calc_quadrupole)
        self.predictor_corrector = None

    def step(self):
        self.predictor_corrector = PredictorCorrector(self.psi, propagator=self.prop, max_steps=2, functionals=totalfunctional)
        self.predictor_corrector()
        self.psi = self.predictor_corrector.psi_pred
        self.rho = self.predictor_corrector.rho_pred
        self.j = self.predictor_corrector.j_pred

    def calc_dipole(self):
        delta_rho = self.rho - self.rho0
        delta_mu = (delta_rho * delta_rho.grid.r).integral()
        self.dipole.append(delta_mu)
    def calc_quadrupole(self):
        delta_rho = self.rho - self.rho0
        q = np.zeros((3,3))
        for i in np.arange(3):
            for j in np.arange(3):
                if j==i:
                    delta = 1
                else:
                    delta = 0
                q[i][j] = ((3 * delta_rho.grid.r[i] * delta_rho.grid.r[j] - (delta_rho.grid.r**2).sum(axis=0) * delta) * delta_rho).integral()
        sprint(len(self.quadrupole), q, comm=self.rho.grid.mp)
        self.quadrupole.append(q)

max_steps = 24175
runner = Runner2(rho_ks, totalfunctional, k, direction, interval, max_steps)

for i, run in enumerate(runner.irun()):
    if i%200==0 and i>1:
        t = np.linspace(0, interval * i, i + 1)
        quadrupole_mu = np.asarray([mu.ravel() for mu in runner.quadrupole])
        delta_mu = np.asarray([mu.ravel() for mu in runner.dipole])
        if mp.is_root:
            print('Saving DATA')
            np.save('KS_LMGP/quadrupole_jp/quadrupole_eopp_'+str(i)+'.npy', np.c_[t[:-1], quadrupole_mu])
            np.save('KS_LMGP/dipole_jp/delta_eopp_'+str(i)+'.npy', np.c_[t[:-1], delta_mu])
